### Chapter 2

**What are the five kinds of brackets?**

- {...}
- {...}
- [...]
- /*..*/
- <...>

---

**What are the two kinds of separators/terminators?**

common & semicolon

---

**How to write comments in C?**

/* comments inside here */

---

**What are Literals?**

Fixed values that are the part of the program.

---

**What are Identifiers?**

"Names" that are given to entities in the program. They could refer to the following

- Data objects aka Variables
- Type aliases such as *size_t*
- Functions such as *main* & *printf*
- Constants such as **EXIT_SUCCESS**

---

**How do identifiers differ from keywords?**

Keywords are predefined by the language and must not be delcared or redefined unlike identifiers.

---

**How is an object defined?**

An object is defined at the same time it is initialized.

---

**What happens to missing elements in initializers?**

They default to 0.

---

**What are statements?**

Statements are instructions that tell the compiler what to do with the idenfitiers.

---

**What are function calls?**

Function calls are special statements that suspend the exeuction of the current function and handover control to the named function.

---
