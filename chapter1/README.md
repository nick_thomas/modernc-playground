### Chapter 1

**What is c99?**

It is the compiler program, like clang, which is used to compile c program to executable.

---

**What does the -Wall flag do?**

It throws warning incase anything went wrong

---

** What is the flag -o do?**

It tells to store the compiler output in the filename followed by the flag

---

**What does the flag -lm do?*

It tells it to add standard mathematical functions if necessary.

---
