### Chapter 3

### Todo Excercises
-A merge sort (with recursion)
-A quick sort (with recursion)”


**What are the conditional control statements in C?**

- if
- for
- do
- while
- switch

---

**What does the value 0 represent as boolean?**

The value 0 represents logical false.

---

**What do all scalars have?**

Truth value.

---

**What are some examples of scalars?**

- size_t
- double
- signed
- unsigned
- bool
- ptrdiff_t
- char const*
- char
- void*
- unsigned char

---

**What is for(;;) equivalent to?**

while(true)

---

**What are the 3 kinds of iteration in C?**

- for
- while
- do/while

---

**What is a break statement?**

It stops the loop without reevaluation the termination condition or exeuction the part of the dependent block after the break statement.

---

**What is a continue statement?**

The continue statement is less frequently used. Like break, it skips the execution of rest of the dependent block.

---
